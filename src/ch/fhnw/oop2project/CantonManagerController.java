package ch.fhnw.oop2project;

import java.net.BindException;

import ch.fhnw.oop2project.Commands.CommandController;
import org.eclipse.jetty.server.Server;

/**
 * The controller of the app.
 * 
 * @author Andreas Gassmann, Jonas Frehner
 */
public class CantonManagerController {
    private final CantonManagerModel model;
    private final CantonManagerView view;

    /**
     * Creates an instance of the controller.
     *
     * @throws Exception exception of the webserver
     */
    public CantonManagerController() throws Exception {
        model = new CantonManagerModel();
        CommandController commandController = new CommandController(model);
        view = new CantonManagerView(this, commandController);
        commandController.setView(view);
        view.setCantons(model.cantons);
        view.initAndShow();
        
        addPropertyChangeListeners();
        startWebserver();
    }

    /**
     * Adds a property change listener.
     */
    public void addPropertyChangeListeners() {
        model.addPropertyChangeListener(evt -> {
            String property = evt.getPropertyName();
            if (property.equals(CantonManagerModel.FLIP_PANEL_INHABITANTS)) {
                view.setFlipInhabitants((int) evt.getNewValue());
            } else if (property.equals(CantonManagerModel.FLIP_PANEL_AREA)) {
                view.setFlipArea((int) evt.getNewValue());
            } else {
                view.setValueAt(evt.getNewValue().toString(), Integer.valueOf(property));
            }
        });
    }

    /**
     * Saves the cantons to the database.
     */
    public void saveCantons() {
        model.db.saveListOfCantons(model.cantons);
    }

    /**
     * Gets a canton by an index (row).
     * 
     * @param row  The index of the canton to get
     */
    public void getCantonByRow(int row) {
        view.setActiveCanton(row, model.getCantonByRow(row));
    }
    
    /**
     * Gets the updated canton by an index (row).
     * Only updates the necessary parts of the view
     * 
     * @param row  The index of the canton to get
     */
    public void getUpdatedCantonByRow(int row) {
        view.setUpdatedActiveCanton(row, model.getCantonByRow(row));
    }

    /**
     * Indicates whether a field has changed in the view and informs the model
     * about the change.
     * 
     * @param currentRow  The row where the changed field belongs to
     * @param fieldid     The id of the changed field
     * @param newValue    The new value that was put in the field
     */
    public void fieldChanged(int currentRow, int fieldid, String newValue) {
        model.fieldChanged(currentRow, fieldid, newValue);
    }
    
    /**
     * Gets the view of the app.
     * 
     * @return  The view
     */
    public CantonManagerView getView() {
        return this.view;
    }

    /**
     * Starts a webserver to access cantons as a list of json by viewing http://localhost:8080/
     *
     * @throws Exception Throws a BindException if the server cannot bind to the port
     */
    public void startWebserver() throws Exception {
        try {
            Server server = new Server(8080);
            server.setHandler(new WebServer(this.model));

            server.start();
            server.join();
        } catch (BindException e) {
            System.out.println("Cannot bind webserver to port...");
        }
    }

    /**
     * Gets the total number inhabitants of switzerland.
     * 
     * @return  int The total number of inhabitants of switzerland
     */
    public int getTotalInhabitants() {
        return model.getTotalInhabitants();
    }

    /**
     * Gets the total area of switzerland.
     * 
     * @return  int The total area of switzerland
     */
    public int getTotalArea() {
        return model.getTotalArea();
    }

}
