package ch.fhnw.oop2project;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The model of the application.
 * Handles all the data and changes to it.
 * 
 * @author Andreas Gassmann, Jonas Frehner
 */
public class CantonManagerModel {

    /**
     * String describing the flip panel inhabitants.
     */
    public final static String FLIP_PANEL_INHABITANTS = "flipPanelInhabitants";

    /**
     * String describing the flip panel area.
     */
    public final static String FLIP_PANEL_AREA = "flipPanelArea";

    /**
     * The database instance.
     */
    public Database db = null;

    /**
     * The list with all the cantons.
     */
    public List<String[]> cantons;

    /**
     * Initialize property change support.
     */
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    /**
     * Initialize database and load the list of cantons
     */
    public CantonManagerModel() {
        db = new Database();
        loadCantons();
    }

    /**
     * Adds a property change listener to the model.
     * 
     * @param listener The propertyChangeListener to add
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.propertyChangeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Removes a propertyChangeListener from the model.
     * 
     * @param listener The propertyChangeListener to remove
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.propertyChangeSupport.removePropertyChangeListener(listener);
    }

    /**
     * Gets all data (cantons) from the given csv-file.
     *
     * @return A list containing arrays of Strings with all the cantons
     */
    public List<String[]> loadCantonsFromFile() {
        List<String[]> tempCantons = new ArrayList<>();
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/ch/fhnw/oop2project/assets/CantonsWithCommunes.csv")));

            while(in.ready()) {
                tempCantons.add(in.readLine().split(";"));
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return tempCantons;
    }

    /**
     * If the database already has data, this will return a list of all cantons.
     * Otherwise it will load the list from a file and store it in the database.
     */
    public void loadCantons() {
        if (db.databaseIsEmpty()) {
            loadCantonsFromFileAndSaveToDb();
        } else {
            cantons = db.loadListOfCantons();
        }
    }

    /**
     * Load the list of cantons from a file and store it in the database.
     */
    public void loadCantonsFromFileAndSaveToDb() {
        db.saveListOfCantons(loadCantonsFromFile());
        cantons = db.loadListOfCantons();
    }

    /**
     * Gets a canton by an index (row).
     * 
     * @param row  The index of the canton to get
     * @return     The canton to get
     */
    public String[] getCantonByRow(int row) {
        return cantons.get(row);
    }

    /**
     * Detects which field has been updated and fires a propertyChange with the
     * data.
     * 
     * @param currentRow  The row of the changed field
     * @param fieldid     The id of the changed field
     * @param newValue    The new value the changed field contains
     */
    public void fieldChanged(int currentRow, int fieldid, String newValue) {
        String oldValue = cantons.get(currentRow)[fieldid];
        cantons.get(currentRow)[fieldid] = newValue;

        if (fieldid == 7) {
            propertyChangeSupport.firePropertyChange(FLIP_PANEL_INHABITANTS, oldValue, getTotalInhabitants());
        }
        if (fieldid == 9) {
            propertyChangeSupport.firePropertyChange(FLIP_PANEL_AREA, oldValue, getTotalArea());
        }

        int tableFieldId;
        switch(fieldid) {
            case 1:
                tableFieldId = 0;
                break;
            case 2:
                tableFieldId = 1;
                break;
            case 5:
                tableFieldId = 2;
                break;
            case 7:
                tableFieldId = 3;
                break;
            case 9:
                tableFieldId = 4;
                break;
            default:
                return;
        }
        propertyChangeSupport.firePropertyChange(Integer.toString(tableFieldId), oldValue, newValue);
    }

    /**
     * Calculates total number of inhabitants.
     *
     * @return Returns total number of inhabitants.
     */
    public int getTotalInhabitants() {
        int total = 0;
        boolean isNegative = false;
        for (int i = 0; i < cantons.size(); i++) {
            try {
                int currentInhabitants = Integer.parseInt(cantons.get(i)[7]);
                if (currentInhabitants < 0) isNegative = true;
                total += currentInhabitants;
            } catch (Exception e) {
                System.out.println("Not a valid number.");
            }
        }
        return isNegative ? -1 : total;
    }

    /**
     * Calculates total area.
     *
     * @return Returns total area.
     */
    public int getTotalArea() {
        int total = 0;
        boolean isNegative = false;
        for (int i = 0; i < cantons.size(); i++) {
            try {
                int currentArea = Integer.parseInt(cantons.get(i)[9]);
                if (currentArea < 0) isNegative = true;
                total += currentArea;
            } catch (Exception e) {
                System.out.println("Not a valid number.");
            }
        }
        return isNegative ? -1 : total;
    }
}
