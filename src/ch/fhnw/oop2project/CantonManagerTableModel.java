/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.fhnw.oop2project;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * The custom Table model to display all the data in a nice table.
 * 
 * @author Andreas Gassmann, Jonas Frehner
 */
public class CantonManagerTableModel extends AbstractTableModel {

    /**
     * The data that should be displayed in the table
     */
    List<String[]> liste;

    /**
     * Creates an instance of this custom table model.
     * 
     * @param liste  The data that should be displayed in the table
     */
    public CantonManagerTableModel(List<String[]> liste) {
        this.liste = liste;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        super.setValueAt(aValue, rowIndex, columnIndex);
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public int getRowCount() {
        return liste.size();
    }

    @Override
    public String getColumnName(int columnId) {
        switch(columnId) {
            case 0:
                return "Kanton";
            case 1:
                return "Kürzel";
            case 2:
                return "Beitritt";
            case 3:
                return "Einwohner";
            case 4:
                return "Fläche";
            default:
                return "";
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch(column) {
            case 0:
                return liste.get(row)[1];
            case 1:
                return liste.get(row)[2];
            case 2:
                return liste.get(row)[5];
            case 3:
                return liste.get(row)[7];
            case 4:
                return liste.get(row)[9];
            default:
                return liste.get(row)[1];
        }
    }
}