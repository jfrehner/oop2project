package ch.fhnw.oop2project;

import javax.swing.UIManager;

/**
 * The class to start the application.
 *
 * @author Andreas Gassmann, Jonas Frehner
 */
public class CantonManager {

    /**
     * The main method of the app. Starts the app.
     *
     * @param args Default java main args
     * @throws Exception exception of the webserver
     */
    public static void main(String[] args) throws Exception {
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        CantonManagerController controller = new CantonManagerController();
    }

}