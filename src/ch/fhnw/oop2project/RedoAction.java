package ch.fhnw.oop2project;

import ch.fhnw.oop2project.Commands.Command;
import ch.fhnw.oop2project.Commands.CommandController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

/**
 * The custom redo actionlistener to execute if a redo operation is started
 * via the redo dropdown menu.
 * 
 * @author Andreas Gassmann, Jonas Frehner
 */
public class RedoAction implements ActionListener {
    
    /**
     * A reference to the commandcontroller.
     */
    private final CommandController commandController;
    
    /**
     * A reference to the view.
     */
    private final CantonManagerView view;
    
    /**
     * A reference to the redoList where all undone commands are stored.
     */
    private final LinkedList<Command> redoList;
    
    /**
     * A reference to the undoList where all executed commands are stored.
     */
    private final LinkedList<Command> undoList;
    
    /**
     * The id ("order") the command was inserted into the redo dropdown menu.
     */
    private final int id;
    
    /**
     * Creates an instance of this custom redoActionListener and initializes
     * all the necessary values.
     * 
     * @param controller  A reference to the controller
     * @param view        A reference to the view
     * @param redoList    A reference to the redoList
     * @param undoList    A reference to the undoList
     * @param id          The id ("ordder") the command was inserted into the
     *                    redo dropdown menu
     */
    public RedoAction(CommandController controller, CantonManagerView view, LinkedList<Command> redoList, LinkedList<Command> undoList, int id) {
        this.commandController = controller;
        this.view = view;
        this.redoList = redoList;
        this.undoList = undoList;
        this.id = id;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        for(int i = redoList.size()-id; i > 0; i--) {
            Command command = redoList.removeLast();
            command.redoExecute();
            undoList.addLast(command);
            if(commandController.hasCantonChanged(command.getCanton())) {
                view.setLedAsChanged(command.getCanton());
            } else {
                view.setLedAsNotChanged(command.getCanton());
            }
            view.setUndoButtonTooltip("Rückgängig: " + command.getMessage());
            if(!redoList.isEmpty() && redoList.getLast().getMessage().length() > 1) {
                view.setRedoButtonTooltip("Wiederholen: " + redoList.getLast().getMessage());
            } else {
                view.setRedoButtonTooltip("");
            }
        }
        if(!redoList.isEmpty()) {
            commandController.redoButtonsNonEmptyRedoList();
        }
        if(redoList.isEmpty()) {
            commandController.redoButtonsEmptyRedoList();
        }
        if(!view.areLedsSet()) {
            view.disableSaveButton();
        }
        commandController.validateCantons();
    }
}