package ch.fhnw.oop2project;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;

/**
 * Manages access to the database
 */
public class Database
{
    /**
     * Timeout of SQL connection
     */
    public final static int TIMEOUT_TIME = 30;

    /**
     * Checks if database is empty
     * @return true of database is empty
     */
    public boolean databaseIsEmpty() {
        return (getCantonById(1) == null);
    }

    /**
     * Recreates the database and initializes it with the cantons provided
     *
     * @param cantons A list with all cantons
     */
    public void saveListOfCantons(List<String[]> cantons) {
        clearDB();
        initDB();
        for (String[] canton : cantons) {
            if (canton[0].equals("Kanton")) continue;
            insertCanton(canton);
        }
    }

    /**
     * Selects all cantons and returns them as a list
     *
     * @return List of cantons
     */
    public List<String[]> loadListOfCantons() {
        List<String[]> cantons = new ArrayList<>();
        for(int i = 1; i < 27; i++) {
            String[] canton = getCantonById(i);
            if (canton[0] != null) cantons.add(canton);
        }
        return cantons;
    }

    /**
     * Selects a canton from the database
     *
     * @param cantonId canton id
     * @return returns a canton
     */
    public String[] getCantonById(int cantonId) {
        return executeStatement(true, "SELECT * FROM cantons WHERE cantonId = " + cantonId + " LIMIT 1");
    }

    /**
     * Drops database
     */
    private void clearDB() {
        executeStatement(false, "DROP TABLE IF EXISTS cantons");
    }

    /**
     * Initializes database
     */
    private void initDB() {
        executeStatement(false, "CREATE TABLE cantons (" +
                "cantonId INTEGER PRIMARY KEY," +
                "cantonName string," +
                "cantonShortname string," +
                "cantonNr string," +
                "cantonVote string," +
                "cantonJoin string," +
                "cantonCapital string," +
                "cantonInhabitants string," +
                "cantonForeigners string," +
                "cantonArea string," +
                "cantonDensity string," +
                "cantonLanguage string," +
                "cantonCommunes string" +
                ")");
    }

    /**
     * Inserts cantons into database
     * @param canton A list of cantons
     */
    private void insertCanton(String[] canton) {
        int i = 0;
        if (canton.length == 13) {
            i++;
        }
        executeStatement(false, "INSERT INTO cantons (" +
                "cantonName," +
                "cantonShortname," +
                "cantonNr," +
                "cantonVote," +
                "cantonJoin," +
                "cantonCapital," +
                "cantonInhabitants," +
                "cantonForeigners," +
                "cantonArea," +
                "cantonDensity," +
                "cantonLanguage," +
                "cantonCommunes) VALUES (" +
                "'" + StringEscapeUtils.escapeSql(canton[i]) + "'," +
                "'" + StringEscapeUtils.escapeSql(canton[1 + i]) + "'," +
                "'" + StringEscapeUtils.escapeSql(canton[2 + i]) + "'," +
                "'" + StringEscapeUtils.escapeSql(canton[3 + i]) + "'," +
                "'" + StringEscapeUtils.escapeSql(canton[4 + i]) + "'," +
                "'" + StringEscapeUtils.escapeSql(canton[5 + i]) + "'," +
                "'" + StringEscapeUtils.escapeSql(canton[6 + i]) + "'," +
                "'" + StringEscapeUtils.escapeSql(canton[7 + i]) + "'," +
                "'" + StringEscapeUtils.escapeSql(canton[8 + i]) + "'," +
                "'" + StringEscapeUtils.escapeSql(canton[9 + i]) + "'," +
                "'" + StringEscapeUtils.escapeSql(canton[10 + i]) + "'," +
                "'" + StringEscapeUtils.escapeSql(canton[11 + i]) + "')");
    }

    /**
     * Executes a mysql statement
     *
     * @param hasResult True if the query should return results
     * @param myStatement mysql statement
     * @return returns a canton if hasResult is true
     */
    private String[] executeStatement(boolean hasResult, String myStatement) {
        String[] result = null;
        Connection connection = null;
        try {
            // create a database connection
            connection = DriverManager.getConnection("jdbc:sqlite:cantons.db");
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(TIMEOUT_TIME);  // set timeout to 30 sec.

            if (hasResult) {
                ResultSet rs = statement.executeQuery(myStatement);
                result = new String[13];
                while(rs.next())
                {
                    result[0] = rs.getString("cantonId");
                    result[1] = rs.getString("cantonName");
                    result[2] = rs.getString("cantonShortname");
                    result[3] = rs.getString("cantonNr");
                    result[4] = rs.getString("cantonVote");
                    result[5] = rs.getString("cantonJoin");
                    result[6] = rs.getString("cantonCapital");
                    result[7] = rs.getString("cantonInhabitants");
                    result[8] = rs.getString("cantonForeigners");
                    result[9] = rs.getString("cantonArea");
                    result[10] = rs.getString("cantonDensity");
                    result[11] = rs.getString("cantonLanguage");
                    result[12] = rs.getString("cantonCommunes");
                }

            } else {
                statement.executeUpdate(myStatement);
            }
        }
        catch(SQLException e) {
            // todo: handle error
            // System.err.println(e.getMessage());
        }
        finally {
            try {
                if(connection != null)
                    connection.close();
            } catch(SQLException e) {
                // connection close failed.
                System.err.println(e);
            }
        }
        return result;
    }
}