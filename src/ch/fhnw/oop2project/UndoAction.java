package ch.fhnw.oop2project;

import ch.fhnw.oop2project.Commands.Command;
import ch.fhnw.oop2project.Commands.CommandController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

/**
 * The undoaction to properly handle clicks on the undo-popup-menu.
 * 
 * @author Andreas Gassmann, Jonas Frehner
 */
public class UndoAction implements ActionListener {

    /**
     * A reference to the commandcontroller.
     */
    private final CommandController commandController;
    
    /**
     * A reference to the view.
     */
    private final CantonManagerView view;
    
    /**
     * A reference to the redoList where all undone commands are stored.
     */
    private final LinkedList<Command> undoList;
    
    /**
     * A reference to the undoList where all executed commands are stored.
     */
    private final LinkedList<Command> redoList;
    
    /**
     * The id ("order") the command was inserted into the redo dropdown menu.
     */
    private final int id;
    
    /**
     * Creates an instance of this custom undoActionListener and initializes
     * all the necessary values.
     * 
     * @param controller  A reference to the controller
     * @param view        A reference to the view
     * @param redoList    A reference to the redoList
     * @param undoList    A reference to the undoList
     * @param id          The id ("ordder") the command was inserted into the
     *                    redo dropdown menu
     */
    public UndoAction(CommandController controller, CantonManagerView view, LinkedList<Command> undoList, LinkedList<Command> redoList, int id) {
        this.commandController = controller;
        this.view = view;
        this.redoList = redoList;
        this.undoList = undoList;
        this.id = id;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        for(int i = undoList.size()-id; i > 0; i--) {
            Command command = undoList.removeLast();
            command.undo();
            redoList.addLast(command);
            if(commandController.hasCantonChanged(command.getCanton())) {
                view.setLedAsChanged(command.getCanton());
            } else {
                view.setLedAsNotChanged(command.getCanton());
            }
            view.setRedoButtonTooltip("Wiederholen: " + command.getMessage());
            if(!undoList.isEmpty() && undoList.getLast().getMessage().length() > 1) {
                view.setUndoButtonTooltip("Rückgängig: " + undoList.getLast().getMessage());
            } else {
                view.setUndoButtonTooltip("");
            }
        }
        if(!undoList.isEmpty()) {
            commandController.undoButtonsNonEmptyUndoList();
        }
        if(undoList.isEmpty()) {
            commandController.undoButtonsEmptyUndoList();
        }
        if(!view.areLedsSet()) {
            view.disableSaveButton();
        }
        commandController.validateCantons();
    }
}