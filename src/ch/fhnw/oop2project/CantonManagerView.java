package ch.fhnw.oop2project;

import ch.fhnw.oop2project.Commands.CommandController;
import java.awt.Image;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import ch.fhnw.oop2project.led.Led;
import ch.fhnw.oop2project.splitflap.GlobalTimer;
import jdk.nashorn.internal.objects.Global;

import java.awt.Color;

/**
 * View of the application.
 * Displays data in a GUI.
 *
 * @author Andreas Gassmann, Jonas Frehner
 */
public class CantonManagerView extends javax.swing.JFrame {

    /**
     * Stores a list of cantons.
     */
    List<String[]> liste = new ArrayList<>();

    /**
     * Defines how many cantons there are.
     */
    private final int NUMBER_OF_CANTONS = 26;

    /**
     * Stores the leds of the cantons.
     */
    private final Led[] ledArray = new Led[NUMBER_OF_CANTONS];

    /**
     * Stores the CantonManagerController.
     */
    private final CantonManagerController controller;

    /**
     * Stores the CommandController.
     */
    private final CommandController commandController;

    /**
     * Store CantonManagerController and CommandController in the class.
     *
     * @param controller CantonManagerController
     * @param commandController CommandController
     */
    public CantonManagerView(CantonManagerController controller, CommandController commandController) {
        this.controller = controller;
        this.commandController = commandController;
    }

    /**
     * Stores a list of cantons.
     *
     * @param cantons A list of cantons.
     */
    public void setCantons(List<String[]> cantons) {
        liste = cantons;
    }

    /**
     * Set the active canton in the GUI.
     *
     * @param row Selected row.
     * @param canton An array of strings containing information about the canton.
     */
    public void setActiveCanton(int row, String[] canton) {
        jTable1.setRowSelectionInterval(row, row);
        try {
            ImageIcon img = new javax.swing.ImageIcon(getClass().getResource("/ch/fhnw/oop2project/img/wappen/" + canton[2] + ".png"));
            Image image = img.getImage();
            Image newimg = image.getScaledInstance(70, 85, java.awt.Image.SCALE_SMOOTH);
            img = new ImageIcon(newimg);
            jLabel2.setIcon(img);
            jLabel2.setText("");
            jLabel4.setText(canton[2]);
        } catch(NullPointerException e) {
            jLabel2.setIcon(null);
            jLabel4.setText("Zu diesem Kanton ist leider kein Wappen vorhanden.");
            jLabel4.setForeground(Color.red);
        }

        jLabel4.setForeground(Color.BLACK);
        jLabel3.setText(canton[1]);
        
        cantonName.setText(canton[1]);
        cantonNr.setText(canton[3]);
        cantonShortname.setText(canton[2]);
        cantonVote.setText(canton[4]);
        cantonInhabitants.setText(canton[7]);
        cantonForeigners.setText(canton[8]);
        cantonCapital.setText(canton[6]);
        cantonJoin.setText(canton[5]);
        cantonArea.setText(canton[9]);
        cantonLanguage.setText(canton[11]);
        jLabel9.setText("Gemeinden (" + canton[12].split(", ").length + ")");
        cantonCommunes.setText(canton[12]);
    }

    /**
     * Flip the Inhabitant panels to the value passed.
     *
     * @param number Number to be displayed.
     */
    public void setFlipInhabitants(int number) {

        if (number == -1) {
            return;
        }

        String s = Integer.toString(number);

        while(s.length() < 8) {
            s = " " + s;
        }

        switch(s.length()) {
            case 8:
                splitFlap1.setText("" + s.charAt(s.length() - 8));
                splitFlap1.setFontColor(Color.WHITE);
            case 7:
                splitFlap2.setText("" + s.charAt(s.length() - 7));
                splitFlap2.setFontColor(Color.WHITE);

                if (number > 999999) {
                    splitFlap3.setText("'");
                } else {
                    splitFlap3.setText(" ");
                }
                splitFlap3.setFontColor(Color.WHITE);
            case 6:
                splitFlap4.setText("" + s.charAt(s.length() - 6));
                splitFlap4.setFontColor(Color.WHITE);
            case 5:
                splitFlap5.setText("" + s.charAt(s.length() - 5));
                splitFlap5.setFontColor(Color.WHITE);
            case 4:
                splitFlap6.setText("" + s.charAt(s.length() - 4));
                splitFlap6.setFontColor(Color.WHITE);

                if (number > 999) {
                    splitFlap7.setText("'");
                } else {
                    splitFlap7.setText(" ");
                }
                splitFlap7.setFontColor(Color.WHITE);

            case 3:
                splitFlap8.setText("" + s.charAt(s.length() - 3));
                splitFlap8.setFontColor(Color.WHITE);
            case 2:
                splitFlap9.setText("" + s.charAt(s.length() - 2));
                splitFlap9.setFontColor(Color.WHITE);
            case 1:
                splitFlap10.setText("" + s.charAt(s.length() - 1));
                splitFlap10.setFontColor(Color.WHITE);
                break;
            case 0:
                break;
        }
    }

    /**
     * Flip the Area panels to the value passed
     *
     * @param area Number to be displayed.
     */
    public void setFlipArea(int area) {

        if (area == -1) {
            return;
        }

        String s = Integer.toString(area);

        while(s.length() < 8) {
            s = " " + s;
        }

        switch(s.length()) {
            case 8:
                splitFlap11.setText("" + s.charAt(s.length() - 8));
                splitFlap11.setFontColor(Color.WHITE);
            case 7:
                splitFlap12.setText("" + s.charAt(s.length() - 7));
                splitFlap12.setFontColor(Color.WHITE);

                if (area > 999999) {
                    splitFlap13.setText("'");
                } else {
                    splitFlap13.setText(" ");
                }
                splitFlap13.setFontColor(Color.WHITE);
            case 6:
                splitFlap14.setText("" + s.charAt(s.length() - 6));
                splitFlap14.setFontColor(Color.WHITE);
            case 5:
                splitFlap15.setText("" + s.charAt(s.length() - 5));
                splitFlap15.setFontColor(Color.WHITE);
            case 4:
                splitFlap16.setText("" + s.charAt(s.length() - 4));
                splitFlap16.setFontColor(Color.WHITE);

                if (area > 999) {
                    splitFlap17.setSelection(new String[]{"'"});
                } else {
                    splitFlap17.setSelection(new String[]{" "});
                }
                splitFlap17.setFontColor(Color.WHITE);

            case 3:
                splitFlap18.setText("" + s.charAt(s.length() - 3));
        splitFlap18.setFontColor(Color.WHITE);
            case 2:
                splitFlap19.setText("" + s.charAt(s.length() - 2));
                splitFlap19.setFontColor(Color.WHITE);
            case 1:
                splitFlap20.setText("" + s.charAt(s.length() - 1));
                splitFlap20.setFontColor(Color.WHITE);
                break;
            case 0:
                break;
        }
    }

    /**
     * If there is an invalid input, other fields will be disabled and an error message will be displayed.
     *
     * @param field Specifies the field containing the error
     * @param message Contains the error message
     */
    public void setError(int field, String message) {
        disableSearchField();
        if (!(field == 1)) cantonName.setEnabled(false);
        if (!(field == 3)) cantonNr.setEnabled(false);
        if (!(field == 2)) cantonShortname.setEnabled(false);
        if (!(field == 4)) cantonVote.setEnabled(false);
        if (!(field == 7)) cantonInhabitants.setEnabled(false);
        if (!(field == 8)) cantonForeigners.setEnabled(false);
        if (!(field == 6)) cantonCapital.setEnabled(false);
        if (!(field == 5)) cantonJoin.setEnabled(false);
        if (!(field == 9)) cantonArea.setEnabled(false);
        if (!(field == 11)) cantonLanguage.setEnabled(false);
        if (!(field == 12)) cantonCommunes.setEnabled(false);

        jLabel10.setText(message);
        switch(field) {
            case 1:
                cantonName.setBackground(Color.red);
                break;
            case 2:
                cantonShortname.setBackground(Color.red);
                break;
            case 3:
                cantonNr.setBackground(Color.red);
                break;
            case 4:
                cantonVote.setBackground(Color.red);
                break;
            case 5:
                cantonJoin.setBackground(Color.red);
                break;
            case 6:
                cantonCapital.setBackground(Color.red);
                break;
            case 7:
                cantonInhabitants.setBackground(Color.red);
                break;
            case 8:
                cantonForeigners.setBackground(Color.red);
                break;
            case 9:
                cantonArea.setBackground(Color.red);
                break;
            case 10:
                //cantonDensity.setBackground(Color.red);
                break;
            case 11:
                cantonLanguage.setBackground(Color.red);
                break;
            case 12:
                cantonCommunes.setBackground(Color.red);
                break;
        }
    }

    /**
     * Enables all input fields after an invalid input has been resolved.
     */
    public void unsetError() {
        enableSearchField();
        cantonName.setEnabled(true);
        cantonNr.setEnabled(true);
        cantonShortname.setEnabled(true);
        cantonVote.setEnabled(true);
        cantonInhabitants.setEnabled(true);
        cantonForeigners.setEnabled(true);
        cantonCapital.setEnabled(true);
        cantonJoin.setEnabled(true);
        cantonArea.setEnabled(true);
        cantonLanguage.setEnabled(true);
        cantonCommunes.setEnabled(true);

        jLabel10.setText("");

        cantonName.setBackground(Color.white);
        cantonShortname.setBackground(Color.white);
        cantonNr.setBackground(Color.white);
        cantonVote.setBackground(Color.white);
        cantonJoin.setBackground(Color.white);
        cantonCapital.setBackground(Color.white);
        cantonInhabitants.setBackground(Color.white);
        cantonForeigners.setBackground(Color.white);
        cantonArea.setBackground(Color.white);
        //cantonDensity.setBackground(Color.white);
        cantonLanguage.setBackground(Color.white);
        cantonCommunes.setBackground(Color.white);
    }

    /**
     * Update labels (title and short name).
     *
     * @param row Currently selected row.
     * @param canton An array of strings containing information about the canton.
     */
    public void setUpdatedActiveCanton(int row, String[] canton) {
        jLabel3.setText(canton[1]);
        jLabel4.setText(canton[2]);
    }

    /**
     * Initializes the view and renders the GUI in another Thread.
     */
    public void initAndShow() {
        java.awt.EventQueue.invokeLater(() -> {
            initComponents();
            addLeds();

            jTable1.getSelectionModel().addListSelectionListener(e -> {
                if (!e.getValueIsAdjusting()) {
                    controller.getCantonByRow(jTable1.getSelectedRow());
                }
            });
            jTable1.setRowSelectionInterval(0, 0); // To select the first row when starting the app
            setFlipInhabitants(controller.getTotalInhabitants());
            setFlipArea(controller.getTotalArea());
            disableSaveButton();
            disableUndoButton();
            disableRedoButton();
            addListeners();
            GlobalTimer.INSTANCE.startTimer();
        });
        this.setVisible(true);
    }

    /**
     * Adds all LEDs
     */
    public void addLeds() {
        for (int i = 0; i < NUMBER_OF_CANTONS; i++) {
            ledArray[i] = new ch.fhnw.oop2project.led.Led();
            ledArray[i].setColor(Color.GREEN);
            ledArray[i].setPreferredSize(new java.awt.Dimension(5, 5));
            jPanel5.add(ledArray[i]);
        }
    }

    /**
     * Sets all LEDs to green.
     */
    public void resetLeds() {
        for (int i = 0; i < NUMBER_OF_CANTONS; i++) {
            ledArray[i].setColor(Color.GREEN);
            ledArray[i].setOn(false);
        }
    }

    /**
     * Disable undo button.
     */
    public void disableUndoButton() {
        undoButton.setEnabled(false);
    }

    /**
     * Enable undo button.
     */
    public void enableUndoButton() {
        undoButton.setEnabled(true);
    }

    /**
     * Disable redo button.
     */
    public void disableRedoButton() {
        redoButton.setEnabled(false);
    }

    /**
     * Enable redo button.
     */
    public void enableRedoButton() {
        redoButton.setEnabled(true);
    }

    /**
     * Disable save button.
     */
    public void disableSaveButton() {
        saveButton.setEnabled(false);
    }

    /**
     * Enable save button.
     */
    public void enableSaveButton() {
        saveButton.setEnabled(true);
    }

    /**
     * Disable search field.
     */
    public void disableSearchField() {
        jTextField1.setEnabled(false);
    }

    /**
     * Enable search field.
     */
    public void enableSearchField() {
        jTextField1.setEnabled(true);
    }

    /**
     * Enables the undo and save button and disables the redo button.
     */
    public void buttonsWhenKeyReleased() {
        enableUndoButton();
        disableRedoButton();
        enableSaveButton();
    }

    /**
     * Disables the undo, redo and save button of the view.
     */
    public void disableAllButtons() {
        disableUndoButton();
        disableRedoButton();
        disableSaveButton();
    }

    /**
     * Enables the undo, redo and save button of the view.
     */
    public void enableAllButtons() {
        enableUndoButton();
        enableRedoButton();
        enableSaveButton();
    }
    
    /**
     * Sets the tooltip of the undo button.
     * 
     * @param tooltip The tooltip to set to the undo button
     */
    public void setUndoButtonTooltip(String tooltip) {
        undoButton.setToolTipText(tooltip);
    }
    
    /**
     * Sets the tooltip of the redo button.
     * 
     * @param tooltip The tooltip to set to the redo button
     */
    public void setRedoButtonTooltip(String tooltip) {
        redoButton.setToolTipText(tooltip);
    }
    
    /**
     * Adds Listeners to all the buttons and text fields of the view.
     */
    public void addListeners() {

        jTextField1.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                jTextField1.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
            }
        });

        jTextField1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                String searchValue = jTextField1.getText();
                int min_row = 0;
                int min_dist = Integer.MAX_VALUE;

                outerLoop:
                for (int row = 0; row <= jTable1.getRowCount() - 1; row++) {
                    for (int col = 0; col <= 2; col++) { // search name, short name and join date
                        int distance = Helpers.LevenshteinDistance(searchValue, jTable1.getValueAt(row, col).toString());
                        if (distance == 0) {
                            min_row = row;
                            min_dist = 0;
                            break outerLoop;
                        } else if(distance <= 2) {
                            if (distance < min_dist) {
                                min_row = row;
                                min_dist = distance;
                            }
                        }
                    }
                }

                if (min_dist < Integer.MAX_VALUE) {
                    jTable1.setRowSelectionInterval(min_row, min_row);
                    jTable1.scrollRectToVisible(jTable1.getCellRect(min_row, 0, true));
                    
                    jLabel3.setEnabled(true);
                    jLabel4.setEnabled(true);
                    cantonName.setEnabled(true);
                    cantonNr.setEnabled(true);
                    cantonShortname.setEnabled(true);
                    cantonVote.setEnabled(true);
                    cantonInhabitants.setEnabled(true);
                    cantonForeigners.setEnabled(true);
                    cantonCapital.setEnabled(true);
                    cantonJoin.setEnabled(true);
                    cantonArea.setEnabled(true);
                    cantonLanguage.setEnabled(true);
                    cantonCommunes.setEnabled(true);
                    jLabel10.setText("");
                } else {
                    jLabel3.setEnabled(false);
                    jLabel4.setEnabled(false);
                    cantonName.setEnabled(false);
                    cantonNr.setEnabled(false);
                    cantonShortname.setEnabled(false);
                    cantonVote.setEnabled(false);
                    cantonInhabitants.setEnabled(false);
                    cantonForeigners.setEnabled(false);
                    cantonCapital.setEnabled(false);
                    cantonJoin.setEnabled(false);
                    cantonArea.setEnabled(false);
                    cantonLanguage.setEnabled(false);
                    cantonCommunes.setEnabled(false);
                    jLabel10.setText("Leider passen keine Ergebnisse auf Ihre Sucheingabe.");
                }
            }
        });

        cantonName.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                commandController.updateField(controller, jTable1.getSelectedRow(), 1, cantonName.getText());
            }
        });

        cantonNr.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                commandController.updateField(controller, jTable1.getSelectedRow(), 3, cantonNr.getText());
            }
        });

        cantonShortname.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                commandController.updateField(controller, jTable1.getSelectedRow(), 2, cantonShortname.getText());
            }
        });

        cantonVote.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                commandController.updateField(controller, jTable1.getSelectedRow(), 4, cantonVote.getText());
            }
        });

        cantonInhabitants.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                commandController.updateField(controller, jTable1.getSelectedRow(), 7, cantonInhabitants.getText());
            }
        });

        cantonForeigners.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                commandController.updateField(controller, jTable1.getSelectedRow(), 8, cantonForeigners.getText());
            }
        });

        cantonCapital.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                commandController.updateField(controller, jTable1.getSelectedRow(), 6, cantonCapital.getText());
            }
        });

        cantonJoin.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                commandController.updateField(controller, jTable1.getSelectedRow(), 5, cantonJoin.getText());
            }
        });

        cantonArea.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                commandController.updateField(controller, jTable1.getSelectedRow(), 9, cantonArea.getText());
            }
        });

        cantonLanguage.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                commandController.updateField(controller, jTable1.getSelectedRow(), 11, cantonLanguage.getText());
            }
        });
        cantonCommunes.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                commandController.updateField(controller, jTable1.getSelectedRow(), 12, cantonCommunes.getText());
            }
        });

        saveButton.addActionListener(e -> commandController.saveCantons(controller));

        undoButton.addActionListener(e -> commandController.undo());
        dropdownUndo.addActionListener(e -> commandController.buildUndoDropdown(undoButton));
        
        redoButton.addActionListener(e -> commandController.redo());
        dropdownRedo.addActionListener(e -> commandController.buildRedoDropdown(redoButton));
    }
    
    /**
     * Sets a value at a specified cell of the table.
     * 
     * @param value  The value to set at the specified location
     * @param column The cell of the table the value should be inserted
     */
    public void setValueAt(String value, int column) {
        jTable1.setValueAt(value, jTable1.getSelectedRow(), column);
    }

    /**
     * Sets an LED of a given index to indicate that some value of the canton
     * has changed. That is, set the color of the LED to red.
     * 
     * @param row The index of the LED to set
     */
    public void setLedAsChanged(int row) {
        ledArray[row].setColor(Color.RED);
        ledArray[row].setOn(true);
    }
    
    /**
     * Sets an LED of a given index as not changed, that is, set the color to a
     * beautiful green.
     * 
     * @param row The index of the LED to set
     */
    public void setLedAsNotChanged(int row) {
        ledArray[row].setColor(Color.GREEN);
        ledArray[row].setOn(false);
    }
    
    /**
     * Tests if any LED is set.
     * 
     * @return True, if any LED is set, false otherwise
     */
    public boolean areLedsSet() {
        for(int i = 0; i < ledArray.length; i++) {
            if(ledArray[i].isOn())
                return true;
        }
        return false;
    }
    
    /**
     * Gets the canton via an index.
     * 
     * @param indexCanton  The index of the canton to get
     * @return             A String array with all of the canton's values
     */
    public String[] getCurrentCanton(int indexCanton) {
        String[] currentViewCanton = new String[11];
        currentViewCanton[0] = cantonName.getText();
        currentViewCanton[1] = cantonShortname.getText();
        currentViewCanton[2] = cantonNr.getText();
        currentViewCanton[3] = cantonVote.getText();
        currentViewCanton[4] = cantonJoin.getText();
        currentViewCanton[5] = cantonCapital.getText();
        currentViewCanton[6] = cantonInhabitants.getText();
        currentViewCanton[7] = cantonForeigners.getText();
        currentViewCanton[8] = cantonArea.getText();
        currentViewCanton[9] = cantonLanguage.getText();
        currentViewCanton[10] = cantonCommunes.getText();
        return currentViewCanton;
    }
    
    /**
     * Gets the jTable of the view.
     * 
     * @return The view's jTable
     */
    public JTable getTable() {
        return jTable1;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jMenuItem1 = new javax.swing.JMenuItem();
        jFrame1 = new javax.swing.JFrame();
        jPanel4 = new javax.swing.JPanel();
        backgroundPanel1 = new ch.fhnw.oop2project.nixienumber.BackgroundPanel();
        jPanel1 = new javax.swing.JPanel();
        saveButton = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        undoButton = new javax.swing.JButton();
        dropdownUndo = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        redoButton = new javax.swing.JButton();
        dropdownRedo = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        cantonName = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        cantonNr = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        cantonShortname = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        cantonVote = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        cantonCapital = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        cantonJoin = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        cantonInhabitants = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        cantonForeigners = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        cantonArea = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        cantonLanguage = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        cantonCommunes = new javax.swing.JTextArea();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        splitFlap1 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap2 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap3 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap4 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap5 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap6 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap7 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap8 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap9 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap10 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        jLabel15 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        splitFlap11 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap12 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap13 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap14 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap15 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap16 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap17 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap18 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap19 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        splitFlap20 = new ch.fhnw.oop2project.splitflap.SplitFlap();
        jLabel19 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();

        jMenuItem1.setText("jMenuItem1");

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setOpaque(false);
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 44));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ch/fhnw/oop2project/img/save-icon.png"))); // NOI18N
        saveButton.setBorderPainted(false);
        saveButton.setContentAreaFilled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel1.add(saveButton, gridBagConstraints);

        jPanel6.setOpaque(false);
        jPanel6.setLayout(new javax.swing.BoxLayout(jPanel6, javax.swing.BoxLayout.LINE_AXIS));

        undoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ch/fhnw/oop2project/img/undo-icon.png"))); // NOI18N
        undoButton.setToolTipText("");
        undoButton.setContentAreaFilled(false);
        undoButton.setMargin(new java.awt.Insets(0, -5, 0, -5));
        jPanel6.add(undoButton);

        dropdownUndo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ch/fhnw/oop2project/img/triangle-small.png"))); // NOI18N
        dropdownUndo.setBorderPainted(false);
        dropdownUndo.setContentAreaFilled(false);
        dropdownUndo.setMaximumSize(new java.awt.Dimension(10, 44));
        dropdownUndo.setMinimumSize(new java.awt.Dimension(10, 44));
        dropdownUndo.setPreferredSize(new java.awt.Dimension(10, 44));
        jPanel6.add(dropdownUndo);

        jPanel1.add(jPanel6, new java.awt.GridBagConstraints());

        jPanel7.setOpaque(false);
        jPanel7.setLayout(new javax.swing.BoxLayout(jPanel7, javax.swing.BoxLayout.LINE_AXIS));

        redoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ch/fhnw/oop2project/img/redo-icon.png"))); // NOI18N
        redoButton.setBorderPainted(false);
        redoButton.setContentAreaFilled(false);
        redoButton.setMargin(new java.awt.Insets(0, -5, 0, -5));
        jPanel7.add(redoButton);

        dropdownRedo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ch/fhnw/oop2project/img/triangle-small.png"))); // NOI18N
        dropdownRedo.setBorderPainted(false);
        dropdownRedo.setContentAreaFilled(false);
        dropdownRedo.setMaximumSize(new java.awt.Dimension(10, 44));
        dropdownRedo.setMinimumSize(new java.awt.Dimension(10, 44));
        dropdownRedo.setPreferredSize(new java.awt.Dimension(10, 44));
        jPanel7.add(dropdownRedo);

        jPanel1.add(jPanel7, new java.awt.GridBagConstraints());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(jLabel1, gridBagConstraints);

        jTextField1.setText("Suchen");
        jTextField1.setToolTipText("Suche");
        jTextField1.setPreferredSize(new java.awt.Dimension(200, 28));
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        jPanel1.add(jTextField1, new java.awt.GridBagConstraints());

        jSplitPane1.setBorder(null);
        jSplitPane1.setResizeWeight(0.9);
        jSplitPane1.setOpaque(false);

        jScrollPane1.setBorder(BorderFactory.createEmptyBorder());
        jScrollPane1.setMaximumSize(new java.awt.Dimension(400, 32767));

        jTable1.setModel(new CantonManagerTableModel(liste));
        jTable1.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(jTable1);

        jSplitPane1.setLeftComponent(jScrollPane1);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ch/fhnw/oop2project/img/wappen/AG.png"))); // NOI18N
        jLabel2.setToolTipText("");

        jLabel3.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel3.setText("jLabel3");

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel4.setText("jLabel4");

        jPanel3.setLayout(new java.awt.GridLayout(5, 4, 10, 3));

        jLabel6.setText("Kanton");
        jPanel3.add(jLabel6);

        cantonName.setText("cantonName");
        cantonName.setPreferredSize(new java.awt.Dimension(0, 0));
        cantonName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantonNameActionPerformed(evt);
            }
        });
        jPanel3.add(cantonName);

        jLabel5.setText("Kantonsnummer");
        jPanel3.add(jLabel5);

        cantonNr.setText("jLabel11");
        jPanel3.add(cantonNr);
        cantonNr.getAccessibleContext().setAccessibleName("cantonNr");

        jLabel8.setText("Kürzel");
        jPanel3.add(jLabel8);

        cantonShortname.setText("cantonShortname");
        cantonShortname.setPreferredSize(new java.awt.Dimension(0, 0));
        jPanel3.add(cantonShortname);

        jLabel14.setText("Standesstimme");
        jPanel3.add(jLabel14);

        cantonVote.setText("cantonVote");
        cantonVote.setPreferredSize(new java.awt.Dimension(0, 0));
        jPanel3.add(cantonVote);

        jLabel13.setText("Hauptort");
        jPanel3.add(jLabel13);

        cantonCapital.setText("cantonCapital");
        cantonCapital.setPreferredSize(new java.awt.Dimension(0, 0));
        jPanel3.add(cantonCapital);

        jLabel17.setText("Beitritt");
        jPanel3.add(jLabel17);

        cantonJoin.setText("cantonJoin");
        cantonJoin.setPreferredSize(new java.awt.Dimension(0, 0));
        jPanel3.add(cantonJoin);

        jLabel18.setText("Einwohner");
        jPanel3.add(jLabel18);

        cantonInhabitants.setText("cantonInhabitants");
        cantonInhabitants.setPreferredSize(new java.awt.Dimension(0, 0));
        jPanel3.add(cantonInhabitants);

        jLabel16.setText("Ausländeranteil");
        jPanel3.add(jLabel16);

        cantonForeigners.setText("cantonForeigners");
        cantonForeigners.setPreferredSize(new java.awt.Dimension(0, 0));
        cantonForeigners.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantonForeignersActionPerformed(evt);
            }
        });
        jPanel3.add(cantonForeigners);

        jLabel12.setText("Fläche in km2");
        jPanel3.add(jLabel12);

        cantonArea.setText("cantonArea");
        cantonArea.setPreferredSize(new java.awt.Dimension(0, 0));
        jPanel3.add(cantonArea);

        jLabel7.setText("Amtssprache");
        jPanel3.add(jLabel7);

        cantonLanguage.setText("cantonLanguage");
        cantonLanguage.setPreferredSize(new java.awt.Dimension(0, 0));
        jPanel3.add(cantonLanguage);

        cantonCommunes.setColumns(20);
        cantonCommunes.setLineWrap(true);
        cantonCommunes.setRows(5);
        cantonCommunes.setWrapStyleWord(true);
        jScrollPane2.setViewportView(cantonCommunes);

        jLabel9.setText("Gemeinden");

        jLabel10.setFont(new java.awt.Font("Lucida Grande", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 0, 0));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel10.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane2)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(jLabel9)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(jLabel2)
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                                                .addGap(18, 18, 18)
                                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                                                                .addComponent(jLabel4)
                                                                                .addGap(0, 0, Short.MAX_VALUE))
                                                                        .addComponent(jLabel3)))
                                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 825, Short.MAX_VALUE))
                                .addContainerGap())))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE))
        );

        jSplitPane1.setRightComponent(jPanel2);

        jPanel5.setMinimumSize(new java.awt.Dimension(800, 0));
        jPanel5.setOpaque(false);
        jPanel5.setPreferredSize(new java.awt.Dimension(800, 133));
        jPanel5.setLayout(new java.awt.GridLayout(1, 0));

        jPanel8.setOpaque(false);
        jPanel8.setLayout(new javax.swing.BoxLayout(jPanel8, javax.swing.BoxLayout.LINE_AXIS));

        jPanel9.setOpaque(false);
        jPanel9.setLayout(new java.awt.GridLayout());
        jPanel9.add(splitFlap1);
        jPanel9.add(splitFlap2);
        jPanel9.add(splitFlap3);
        jPanel9.add(splitFlap4);
        jPanel9.add(splitFlap5);
        jPanel9.add(splitFlap6);
        jPanel9.add(splitFlap7);
        jPanel9.add(splitFlap8);
        jPanel9.add(splitFlap9);
        jPanel9.add(splitFlap10);

        jPanel8.add(jPanel9);

        jLabel15.setFont(new java.awt.Font("Lucida Grande", 1, 22)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel15.setText("Einwohner  ");
        jLabel15.setMaximumSize(new java.awt.Dimension(200, 27));
        jLabel15.setMinimumSize(new java.awt.Dimension(200, 27));
        jLabel15.setPreferredSize(new java.awt.Dimension(200, 27));
        jPanel8.add(jLabel15);

        jPanel10.setOpaque(false);
        jPanel10.setLayout(new javax.swing.BoxLayout(jPanel10, javax.swing.BoxLayout.LINE_AXIS));

        jPanel11.setOpaque(false);
        jPanel11.setLayout(new java.awt.GridLayout());
        jPanel11.add(splitFlap11);
        jPanel11.add(splitFlap12);
        jPanel11.add(splitFlap13);
        jPanel11.add(splitFlap14);
        jPanel11.add(splitFlap15);
        jPanel11.add(splitFlap16);
        jPanel11.add(splitFlap17);
        jPanel11.add(splitFlap18);
        jPanel11.add(splitFlap19);
        jPanel11.add(splitFlap20);

        jPanel10.add(jPanel11);

        jLabel19.setFont(new java.awt.Font("Lucida Grande", 1, 22)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel19.setText("km² ");
        jLabel19.setMaximumSize(new java.awt.Dimension(200, 27));
        jLabel19.setMinimumSize(new java.awt.Dimension(200, 27));
        jLabel19.setPreferredSize(new java.awt.Dimension(200, 27));
        jPanel10.add(jLabel19);

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ch/fhnw/oop2project/img/wappen/switzerland.png"))); // NOI18N
        jLabel11.setBorder(javax.swing.BorderFactory.createEmptyBorder(10, 10, 10, 10));

        javax.swing.GroupLayout backgroundPanel1Layout = new javax.swing.GroupLayout(backgroundPanel1);
        backgroundPanel1.setLayout(backgroundPanel1Layout);
        backgroundPanel1Layout.setHorizontalGroup(
            backgroundPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(backgroundPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(backgroundPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 873, Short.MAX_VALUE)
                        .addComponent(jSplitPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, backgroundPanel1Layout.createSequentialGroup()
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(backgroundPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, 687, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        backgroundPanel1Layout.setVerticalGroup(
            backgroundPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(backgroundPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSplitPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 453, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(backgroundPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(backgroundPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, backgroundPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel11))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(backgroundPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(backgroundPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cantonForeignersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantonForeignersActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantonForeignersActionPerformed

    private void cantonNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantonNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantonNameActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ch.fhnw.oop2project.nixienumber.BackgroundPanel backgroundPanel1;
    private javax.swing.JTextField cantonArea;
    private javax.swing.JTextField cantonCapital;
    private javax.swing.JTextArea cantonCommunes;
    private javax.swing.JTextField cantonForeigners;
    private javax.swing.JTextField cantonInhabitants;
    private javax.swing.JTextField cantonJoin;
    private javax.swing.JTextField cantonLanguage;
    private javax.swing.JTextField cantonName;
    private javax.swing.JLabel cantonNr;
    private javax.swing.JTextField cantonShortname;
    private javax.swing.JTextField cantonVote;
    private javax.swing.JButton dropdownRedo;
    private javax.swing.JButton dropdownUndo;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JButton redoButton;
    private javax.swing.JButton saveButton;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap1;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap10;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap11;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap12;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap13;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap14;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap15;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap16;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap17;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap18;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap19;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap2;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap20;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap3;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap4;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap5;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap6;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap7;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap8;
    private ch.fhnw.oop2project.splitflap.SplitFlap splitFlap9;
    private javax.swing.JButton undoButton;
    // End of variables declaration//GEN-END:variables
}
