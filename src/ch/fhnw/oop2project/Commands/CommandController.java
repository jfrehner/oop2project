package ch.fhnw.oop2project.Commands;

import ch.fhnw.oop2project.CantonManagerController;
import ch.fhnw.oop2project.CantonManagerModel;
import ch.fhnw.oop2project.CantonManagerView;
import ch.fhnw.oop2project.RedoAction;
import ch.fhnw.oop2project.UndoAction;

import java.util.LinkedList;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * Controls all the available Commands.
 *
 * @author Andreas Gassmann, Jonas Frehner
 */
public class CommandController {

    private final LinkedList<Command> undoList = new LinkedList<>();
    private final LinkedList<Command> redoList = new LinkedList<>();

    private final CantonManagerModel model;
    private CantonManagerView view;

    /**
     * Creates an instance of the CommandController.
     *
     * @param model CantonManagerModel
     */
    public CommandController(CantonManagerModel model) {
        this.model = model;
    }

    /**
     * Passes a reference to the current view to the CommandController.
     *
     * @param view The reference to the view.
     */
    public void setView(CantonManagerView view) {
        this.view = view;
    }

    /**
     * Updates a field.
     *
     * @param controller A reference to the controller
     * @param selectedRow The currently selected table row
     * @param column The column in which the value has changed
     * @param value The new value of the field
     */
    public void updateField(CantonManagerController controller, int selectedRow, int column, String value) {
        String oldValue = model.cantons.get(selectedRow)[column];
        if (oldValue.equals(value)) {
            return; // If the value doesn't change, we won't save/execute the command
        }
        UpdateFieldCommand updateFieldCommand = new UpdateFieldCommand(controller, selectedRow, column, oldValue, value);
        undoList.addLast(updateFieldCommand);
        redoList.clear();
        updateFieldCommand.execute();

        validateInput(column, value);

        view.setUndoButtonTooltip("Rückgängig: " + updateFieldCommand.getMessage());
        view.buttonsWhenKeyReleased();
        view.setLedAsChanged(selectedRow);
        validateCantons();
    }

    /**
     * Saves the cantons.
     *
     * @param controller A reference to the controller
     */
    public void saveCantons(CantonManagerController controller) {
        SaveCantonsCommand saveCantonsCommand = new SaveCantonsCommand(controller);
        saveCantonsCommand.execute();
        view.resetLeds();
        view.disableSaveButton();
    }
    
    /**
     * Builds the dropdown-menu for the undo-triangle button.
     * 
     * @param button  The undo button the popupmenu belongs to
    */
    public void buildUndoDropdown(JButton button) {
        JPopupMenu undoMenu = new JPopupMenu("UndoDropdown");
        if(undoList.size() >= 10) {
            for(int i = undoList.size(); i >= undoList.size()-10; i--) {
                JMenuItem item = new JMenuItem(undoList.get(i-1).getMessage());
                item.addActionListener(new UndoAction(this, view, undoList, redoList, i));
                undoMenu.add(item);
            }
        }
        if(undoList.size() < 10) {
            for(int i = undoList.size()-1; i >= 0; i--) {
                JMenuItem item = new JMenuItem(undoList.get(i).getMessage());
                item.addActionListener(new UndoAction(this, view, undoList, redoList, i));
                undoMenu.add(item);
            }
        }
        undoMenu.show(button, 0, button.getHeight());
    }
    
    /**
     * Builds the dropdown-menu for the redo-triangle button.
     * 
     * @param button  The redo button the popupmenu belongs to
    */
    public void buildRedoDropdown(JButton button) {
        JPopupMenu redoMenu = new JPopupMenu("RedoDropdown");
        if(redoList.size() >= 10) {
            for(int i = redoList.size(); i >= redoList.size()-10; i--) {
                JMenuItem item = new JMenuItem(redoList.get(i-1).getMessage());
                item.addActionListener(new RedoAction(this, view, redoList, undoList, i));
                redoMenu.add(item);
            }
        }
        if(redoList.size() < 10) {
            for(int i = redoList.size()-1; i >= 0; i--) {
                JMenuItem item = new JMenuItem(redoList.get(i).getMessage());
                item.addActionListener(new RedoAction(this, view, redoList, undoList, i));
                redoMenu.add(item);
            }
        }
        redoMenu.show(button, 0, button.getHeight());
    }

    /**
     * Undoes a command and enables/disables the redo-, undo- and save-buttons.
     */
    public void undo() {
        if (!undoList.isEmpty()) {
            Command command = undoList.removeLast();
            command.undo();
            redoList.addLast(command);
            validateInput(command.getColumn(), command.getOldValue());
            view.enableAllButtons();
            if(hasCantonChanged(command.getCanton())) {
                view.setLedAsChanged(command.getCanton());
            } else {
                view.setLedAsNotChanged(command.getCanton());
            }
            view.setRedoButtonTooltip("Wiederholen: " + command.getMessage());
            if(!undoList.isEmpty() && undoList.getLast().getMessage().length() > 1) {
                view.setUndoButtonTooltip("Rückgängig: " + undoList.getLast().getMessage());
            } else {
                view.setUndoButtonTooltip("");
            }
        }
        if (undoList.isEmpty()) {
            undoButtonsEmptyUndoList();
        }
        if(!view.areLedsSet()) {
            view.disableSaveButton();
        }
        validateCantons();
    }
    
    /**
     * Redoes a command and enables/disables the redo-, undo- and save-buttons.
     */
    public void redo() {
        if (!redoList.isEmpty()) {
            Command command = redoList.removeLast();
            command.redoExecute();
            undoList.addLast(command);
            validateInput(command.getColumn(), command.getValue());
            if(hasCantonChanged(command.getCanton())) {
                view.setLedAsChanged(command.getCanton());
            } else {
                view.setLedAsNotChanged(command.getCanton());
            }
            view.setUndoButtonTooltip("Rückgängig: " + command.getMessage());
            if(!redoList.isEmpty() && redoList.getLast().getMessage().length() > 1) {
                view.setRedoButtonTooltip("Wiederholen: " + redoList.getLast().getMessage());
            } else {
                view.setRedoButtonTooltip("");
            }
            view.enableAllButtons();
        }
        if (redoList.isEmpty()) {
            redoButtonsEmptyRedoList();
        }
        if(!view.areLedsSet()) {
            view.disableSaveButton();
        }
        validateCantons();
    }
    
    /**
     * Indicates whether values of a given canton have changed.
     * 
     * @param affectedCanton  The index of the canton to test
     * @return boolean        Returns true if some values of the canton have changed
     *                        false otherwise
     */
    public boolean hasCantonChanged(int affectedCanton) {
        
        String[] currentViewCanton = view.getCurrentCanton(affectedCanton);
        String[] cantonFromDB = model.db.getCantonById(affectedCanton + 1);

        return !(currentViewCanton[0].equals(cantonFromDB[1]) &&
                currentViewCanton[1].equals(cantonFromDB[2]) &&
                currentViewCanton[2].equals(cantonFromDB[3]) &&
                currentViewCanton[3].equals(cantonFromDB[4]) &&
                currentViewCanton[4].equals(cantonFromDB[5]) &&
                currentViewCanton[5].equals(cantonFromDB[6]) &&
                currentViewCanton[6].equals(cantonFromDB[7]) &&
                currentViewCanton[7].equals(cantonFromDB[8]) &&
                currentViewCanton[8].equals(cantonFromDB[9]) &&
                currentViewCanton[9].equals(cantonFromDB[11]) &&
                currentViewCanton[10].equals(cantonFromDB[12]));
    }
    
    /**
     * Helper function to enable or disable the buttons when an undo action was
     * executed and there are still undoable commands left.
     */
    public void undoButtonsEmptyUndoList() {
        view.disableUndoButton();
        view.enableRedoButton();
        view.enableSaveButton();
    }
    
    /**
     * Helper function to enable or disable the buttons when an undo action was
     * executed and there are no undoable commands left.
     */
    public void undoButtonsNonEmptyUndoList() {
        view.enableAllButtons();
    }
    
    /**
     * Helper function to enable or disable the buttons when a redo action was
     * executed and there are redoable commands left.
     */
    public void redoButtonsEmptyRedoList() {
        view.enableUndoButton();
        view.disableRedoButton();
        view.enableSaveButton();
    }
    
    /**
     * Helper function to enable or disable the buttons when a redo action was
     * executed and there are no redoable commands left.
     */
    public void redoButtonsNonEmptyRedoList() {
        view.enableAllButtons();
    }
    
    public void validateCantons() {
        for(int i = 0; i < model.cantons.size(); i++) {
            String[] canton = model.getCantonByRow(i);
            if(!validateFields(canton)) {
                view.disableSaveButton();
                view.getTable().setEnabled(false);
                return;
            } else {
                view.getTable().setEnabled(true);
            }
        }
    }
    
    /**
     * Validates all the fields of a canton. Returns false if one ore more fields
     * of a canton have changed, true otherwise.
     * 
     * @param canton  The canton in form of an array.
     * @return        Returns true if no fields of a canton have changed.
     */
    private boolean validateFields(String[] canton) {
        if(validateInput(1, canton[1]) || validateInput(2, canton[2]) || validateInput(3, canton[3])
         || validateInput(4, canton[4]) || validateInput(5, canton[5]) || validateInput(6, canton[6])
          || validateInput(7, canton[7]) || validateInput(8, canton[8]) || validateInput(9, canton[9])
           || validateInput(10, canton[10]) || validateInput(11, canton[11]) || validateInput(12, canton[12])) {
            return false;
        }
        return true;
    }

    /**
     * Validates the input in a field.
     * Prints out a handy error message and calls the setError() method to process
     * the error.
     * 
     * @param field     The field of which the input should be validated
     * @param input     The value to validate
     * @return Boolean  Returns true if the input is valid, false otherwise
     */
    private boolean validateInput(int field, String input) {
        String message = null;

        switch(field) {
            case 1: // canton name
                if (input.length() == 0) {
                    message = "Dieses Feld darf nicht leer sein!";
                    view.setError(field, message);
                } else if (input.length() > 0) {
                    view.unsetError();
                }
                break;
            case 2: // canton shortname
                if (input.length() != 2) {
                    message = "Dieses Feld muss genau zwei Zeichen lang sein!";
                    view.setError(field, message);
                } else if (input.length() == 2) {
                    view.unsetError();
                }
                break;
            case 3: // canton nr
                if (!isInteger(input) || input.isEmpty() || input.charAt(0) == '-') {
                    message = "Dieses Feld muss eine positive Zahl sein!";
                    view.setError(field, message);
                } else if (isInteger(input)) {
                    view.unsetError();
                }
                break;
            case 4: // canton vote
                if (!isFloat(input) || input.isEmpty() || input.charAt(0) == '-') {
                    message = "Dieses Feld muss eine positive Zahl sein!";
                    view.setError(field, message);
                } else if (isFloat(input)) {
                    view.unsetError();
                }
                break;
            case 5: // canton join
                if (!isInteger(input) || input.isEmpty()) { // This value could be negative
                    message = "Dieses Feld muss eine Zahl sein!";
                    view.setError(field, message);
                } else if (isInteger(input)) {
                    view.unsetError();
                }
                break;
            case 6: // canton capital
                if (input.length() == 0) {
                    message = "Dieses Feld darf nicht leer sein!";
                    view.setError(field, message);
                } else if (input.length() > 0) {
                    view.unsetError();
                }
                break;
            case 7: // canton inhabitants
                if (!isInteger(input) || input.isEmpty() || input.charAt(0) == '-') {
                    message = "Dieses Feld muss eine positive Zahl sein!";
                    view.setError(field, message);
                } else if (isInteger(input)) {
                    view.unsetError();
                }
                break;
            case 8: // canton foreigners
                if (!isFloat(input) || input.isEmpty() || input.charAt(0) == '-') {
                    message = "Dieses Feld muss eine positive Zahl sein!";
                    view.setError(field, message);
                } else if (isFloat(input)) {
                    view.unsetError();
                }
                break;
            case 9: // canton area
                if (input.isEmpty() || !isInteger(input) || input.charAt(0) == '-') {
                    message = "Dieses Feld muss eine positive Zahl sein!";
                    view.setError(field, message);
                } else if (isInteger(input)) {
                    view.unsetError();
                }
                break;
            case 10: // canton density
                if (input.isEmpty() || !isInteger(input) || input.charAt(0) == '-') {
                    message = "Dieses Feld muss eine positive Zahl sein!";
                    view.setError(field, message);
                }
                if (isInteger(input)) {
                    view.unsetError();
                }
                break;
            case 11: // canton language
                break;
            case 12: // canton communes
                break;
        }
        return message != null;
    }

    /**
     * Tests if the param is an integer or not.
     * 
     * @param s  The String to test whether it is an integer
     * @return   Returns true if the param is an integer, false otherwise
     */
    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        return true;
    }

    /**
     * Tests if the param is a float or not.
     * 
     * @param s  The string to test whether it is a float
     * @return   Returns true if the param is a float, false otherwise
     */
    public static boolean isFloat(String s) {
        try {
            Float.parseFloat(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        return true;
    }
}