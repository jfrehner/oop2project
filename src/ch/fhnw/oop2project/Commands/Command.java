package ch.fhnw.oop2project.Commands;

/**
 * Interface for a Command.
 *
 * @author Andreas Gassmann, Jonas Frehner
 */
public interface Command {

    /**
     * Abstract method to execute.
     */
    void execute();

    /**
     * Abstract method to execute if the redo execute is different than the
     * normal one.
     */
    void redoExecute();
    
    /**
     * Abstract undo()-method to undo the command.
     */
    void undo();
    
    /**
     * Gets the affected canton.
     *
     * @return Returns the affected canton.
     */
    int getCanton();
    
    /**
     * Gets the undo- or redo-Message of the command.
     *
     * @return Returns the message.
     */
    String getMessage();
    
    /**
     * Gets the old value.
     *
     * @return Returns the old value.
     */
    String getOldValue();
    
    /**
     * Gets the column.
     *
     * @return Returns the column.
     */
    int getColumn();
    
    /**
     * Gets the new value.
     *
     * @return Returns the value.
     */
    String getValue();
}
