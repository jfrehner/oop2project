package ch.fhnw.oop2project.Commands;

import ch.fhnw.oop2project.CantonManagerController;
import ch.fhnw.oop2project.CantonManagerModel;
import ch.fhnw.oop2project.CantonManagerView;

/**
 * The command object to update a field.
 *
 * @author Andreas Gassmann, Jonas Frehner
 */
public class UpdateFieldCommand implements Command {

    private final CantonManagerController controller;
    private final int selectedRow;
    private final int column;
    private final String value;
    private String oldValue;
    private String message;

    /**
     * Creates an object of an updateFieldCommand.
     *
     * @param controller A reference to the controller
     * @param selectedRow The currently selected row
     * @param column The field of the canton to be updated
     * @param oldValue The old value of the field
     * @param value The new value of the field
     */
    public UpdateFieldCommand(CantonManagerController controller, int selectedRow, int column, String oldValue, String value) {
        this.controller = controller;
        this.value = value;
        this.column = column;
        this.selectedRow = selectedRow;
        this.oldValue = oldValue;
    }

    @Override
    public void execute() {
        controller.fieldChanged(selectedRow, column, value);
        controller.getUpdatedCantonByRow(selectedRow);
    }

    @Override
    public void redoExecute() {
        controller.fieldChanged(selectedRow, column, value);
        controller.getCantonByRow(selectedRow);
    }
    
    @Override
    public void undo() {
        controller.fieldChanged(selectedRow, column, oldValue);
        controller.getCantonByRow(selectedRow);
    }
    
    @Override
    public int getCanton() {
        return this.selectedRow;
    }
    
    @Override
    public String getMessage() {
        switch(column) {
            case 1:
                message = "Feld \"Kanton\" verändern";
                break;
            case 2:
                message = "Feld \"Kürzel\" verändern";
                break;
            case 3:
                message = "Feld \"Kantonsnummer\" verändern";
                break;
            case 4:
                message = "Feld \"Standesstimme\" verändern";
                break;
            case 5:
                message = "Feld \"Beitritt\" verändern";
                break;
            case 6:
                message = "Feld \"Hauptort\" verändern";
                break;
            case 7:
                message = "Feld \"Einwohner\" verändern";
                break;
            case 8:
                message = "Feld \"Ausländer\" verändern";
                break;
            case 9:
                message = "Feld \"Fläche\" verändern";
                break;
            case 12:
                message = "Feld \"Gemeinden\" verändern";
                break;
            case 11:
                message = "Feld \"Amtssprache\" verändern";
                break;
        }
        return message;
    }
    
    @Override
    public String getOldValue() {
        return oldValue;
    }
    
    @Override
    public String getValue() {
        return value;
    }
    
    @Override
    public int getColumn() {
        return column;
    }
    
}