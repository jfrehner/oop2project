package ch.fhnw.oop2project.Commands;

import ch.fhnw.oop2project.CantonManagerController;

/**
 * The command object to save all the cantons.
 *
 * @author Jonas Frehner
 */
public class SaveCantonsCommand implements Command {

    private final CantonManagerController controller;

    /**
     * Creates an object of a SaveCantonsCommand.
     *
     * @param controller A reference to the controller
     */
    public SaveCantonsCommand(CantonManagerController controller) {
        this.controller = controller;
    }

    @Override
    public void execute() {
        controller.saveCantons();
    }

    @Override
    public void undo() {}
    
    @Override
    public void redoExecute() {}
    
    @Override
    public int getCanton() {
        return -99;
    }
    
    @Override
    public String getMessage() {
        return "";
    }
    
    @Override
    public String getOldValue() {
        return "";
    }
    
    @Override
    public String getValue() {
        return "";
    }
    
    @Override
    public int getColumn() {
        return -99;
    }
}