package ch.fhnw.oop2project;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

import java.io.IOException;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

/**
 * A webserver that returns a list of all cantons
 */
public class WebServer extends AbstractHandler {

    /**
     * The CantonManagerModel
     */
    private CantonManagerModel model;

    /**
     * Save the CantonManagerModel inside the class
     *
     * @param model CantonManagerModel
     */
    public WebServer(CantonManagerModel model) {
        this.model = model;
    }

    /**
     * Handle the requests to the webserver. Send all cantons as json to the browser.
     *
     * @param target Target of the request
     * @param baseRequest Request
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @throws IOException IOException
     * @throws ServletException ServletException
     */
    public void handle(String target, Request baseRequest, HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("application/json;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);

        String[] cantonArray = new String[model.cantons.size()];

        for (int i = 0; i < model.cantons.size(); i++) {
            String[] fieldArray = new String[13];
            fieldArray[0] = "\"id\": \"" + model.cantons.get(i)[0] + "\"";
            fieldArray[1] = "\"Kanton\": \"" + model.cantons.get(i)[1] + "\"";
            fieldArray[2] = "\"Kürzel\": \"" + model.cantons.get(i)[2] + "\"";
            fieldArray[3] = "\"Kantonsnummer\": \"" + model.cantons.get(i)[3] + "\"";
            fieldArray[4] = "\"Standesstimme\": \"" + model.cantons.get(i)[4] + "\"";
            fieldArray[5] = "\"Beitritt\": \"" + model.cantons.get(i)[5] + "\"";
            fieldArray[6] = "\"Hauptort\": \"" + model.cantons.get(i)[6] + "\"";
            fieldArray[7] = "\"Einwohner\": \"" + model.cantons.get(i)[7] + "\"";
            fieldArray[8] = "\"Ausländer\": \"" + model.cantons.get(i)[8] + "\"";
            fieldArray[9] = "\"Fläche\": \"" + model.cantons.get(i)[9] + "\"";
            fieldArray[10] = "\"Einwohnerdichte\": \"" + model.cantons.get(i)[10] + "\"";
            fieldArray[11] = "\"Amtssprache\": \"" + model.cantons.get(i)[11] + "\"";
            fieldArray[12] = "\"Gemeinden\": \"" + model.cantons.get(i)[12] + "\"";

            cantonArray[i] = "{" + String.join(", ", fieldArray) + "}";
        }

        String JSONResponse = "[" + String.join(", ", cantonArray) + "]";

        response.getWriter().println(JSONResponse);
    }
}
